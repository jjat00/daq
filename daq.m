s = daq.createSession('ni');
addAnalogInputChannel(s,'Dev1','ai0','Voltage');
s.IsContinuous = true;
s.Rate=5000;
data=linspace(-1,1,5000)';
lh = addlistener(s,'DataAvailable',@plotData);
startBackground(s)
global h
h = [4564 23456 7654 23456 874 234 567 23 543 1234 54 123 654 23 6 23 23];
 function plotData(src,event)
    global h
     subplot(2,1,1)
     plot(event.TimeStamps,event.Data,'r')
    % ylim([-5 5])
     c = conv(h,event.Data);
     subplot(2,1,2)
     plot(c,'b')
     %xlim([10 100])
     %ylim([-5 5])
     length(event.Data)
     %getF(event.Data,1000);
 end