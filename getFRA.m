function [f0,A,M]=getFRA(dato,fs,M)
% f0 frecuencia de la se�al
%A amplitud de la se�al durante el periodo ventana, valido para una se�al
%sin offset
%M matris que sirve de memoria , en M(1) guarda la frecuencia , M(2) se
%guarda la amplitud
N=10*fs;
if( N+2~=length(M))
    M=zeros(1,N+2);
end
M(3:end)=[dato,M(3:end-1)];
z=abs(fft(M(3:end))/N);
z2=z(1:N/2+1);
z2(2:end-1) = 2*z2(2:end-1);
f = fs*(0:(N/2))/N;
[~,i]=max(z2);
Am=max(M(3:end));
M(1)=f(i);
M(2)=Am;
f0=M(1);
A=M(2);