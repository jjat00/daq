function [a,b]=filtro(wp,ws,rp,rs,fs)
% wp=40; %banda de paso
% ws=150;%banda de parada
% rp=3;%rizado en db
% rs=60;% atenuacion en la banda de parada
% fs=1000/2;%frecuenca de muestreo
[n,wn]=buttord(wp/fs,ws/fs,rp,rs);%hay que entregar las bandas normalizadas con respecto a la frecuencia de muestre0
[b,a]=butter(n,wn);
% a=a*100;
% b=b*100;
%freqz(b,a,150,fs*2)%150 datos 
% a=a(1)*a(2:end);
% b=b/a(1);