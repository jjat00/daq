s = daq.createSession('ni');
s1 = daq.createSession('ni');
addAnalogInputChannel(s,'Dev1','ai0','Voltage');
addAnalogInputChannel(s1,'Dev1','ai2','Voltage');
%s.DurationInSeconds = 0.1;
%s.Rate = 512;
%s.NumberOfScans = 256;
%% VECTOR UTILIZADO EN LA CONVOLUCION
h=[0 1 0];
cont=0;
M=0;
M1=0;
Y=0;
%GRAFICA
axes(handles.axes1);
a0 = animatedline('Color',([0.7214 0.7521 0.3543]),'linewidth',1.3); % Se�al de entrada
a01 = animatedline('Color',([0.114 0.4521 0.3543]),'linewidth',1.3); % Se�al de entrada
%a1 = animatedline('Color','b','linewidth',1); % Convolution 
a1 = animatedline('Color',([0.7243 0.2214 0.2132]),'linewidth',1.3);
a2 = animatedline('Color',rand(1,3),'linewidth',1.3); % IIR
legend({'x(t)','x1(t)','Convolution','IIR'},'FontSize',10,'TextColor','w','Location','southwest')
legend('boxoff')
set(gca,'Color','k','GridColor','w','XColor',([0.7214 0.7521 0.6543]),'Ycolor','k');
title('DAQ Signal','Color',([0.2214 0.7521 0.3543]));
xlabel('Time(s)','Color',([0.2414 0.6821 0.2543]));
ylabel('Amplitude(V)','Color',([0.2414 0.6821 0.2543]));
axis auto
%ylim([-5 5]);  
grid on;
%%  PETICION DEL DATO 
global q 
pause on
tic;
fs = 13;
%[a,b]=filtro(0.4,10,3,60,fs)%%%%%%%%%%%%%%%
a=[1.2];
b=[1];
 a(2,1:end)=zeros(1,end);%%%%%%%%%%%%%%%%
 b(2,1:end)=zeros(1,end);%%%%%%%%%%%%%%%
q = 0;
cont = 0;
r1 = 0;
r2 = 0;
r3 = 0;
op4 = 0;
while(1)
op1 = get(handles.radiobutton1,'Value');
op2 = get(handles.radiobutton3,'Value');
op3 = get(handles.radiobutton2,'Value');
op4 = get(handles.radiobutton4,'Value');
if (op1 == 1)
    r1 = (get(handles.slider1,'Value'))
end
if (op2 == 1)
    r2 = (get(handles.slider1,'Value'))
end
if (op3 == 1)
    r3 = (get(handles.slider1,'Value'))
end
    [data,triggerTime] = inputSingleScan(s); 
    [data1,triggerTime1] = inputSingleScan(s1);
    add = data+data1;
    t = toc 
    if (op4 == 0)
        addpoints(a0,t,data+r1);   % Entrada x(t)
        [Y,M]=getConvolucion(data,h,M);
        [y,a,b]=getY(data,a,b);
        [f0,A,M1]=getFRA(data,fs,M1);
    end   
    if (op4 == 1)
        addpoints(a01,t,add+r1);   % Entrada x1(t)
        [Y,M]=getConvolucion(add,h,M);
        [y,a,b]=getY(add,a,b);
        [f0,A,M1]=getFRA(add,fs,M1);
    end     
    addpoints(a1,t,Y+r2);    % Convoluci�n
    addpoints(a2,t,y+r3);   % IIR
    if t >= 10
        xlim([t-10 t]);    
    end
    drawnow
    set(handles.textVoltR,'String',data);
    set(handles.textFr,'String',f0)
    set(handles.textA,'String',A)
    cont = cont+1
    if(data>=10)
        break
    end
    if q == 1
           break;
    end 
    pause(0.00000001);
end