function f0=getF(datos,fs)
N=length(datos)
z=abs(fft(datos)./N);
z2=z(1:N/2+1);
z2(2:end-1) = 2.*z2(2:end-1);
f = fs.*(0:(N/2))./N;
[M,i]=max(z2);
f0=f(i)
