function [Y,M]=getConvolucion(data,h,M)
%%LA funcion necesita de una memoria (M) del tama�o de h
%% INICIALIZACION DE MATRICES de valores
nh=length(h);
if(length(M)~=nh)
  M=zeros(nh,nh);  
end
%%operaciones
m = data*h;
M(nh,1:nh)=m;
    y=sum(M);
    Y=y(1);
%%rotacion
    for i=1:nh-1
        M(i,1:i)=M(i+1,2:i+1);
    end