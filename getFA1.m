function [f0,A,M]=getFA1(dato,fs,M)
% f0 frecuencia de la se�al
%A amplitud de la se�al durante el periodo ventana, valido para una se�al
%sin offset
%M matris que sirve de memoria , en M(1) guarda la frecuencia , M(2) se
%guarda la amplitud

if(length(M)>=fs+2)
N=fs
z=abs(fft(M(3:end))/N)
z2=z(1:N/2+1)
z2(2:end-1) = 2*z2(2:end-1)
f = fs*(0:(N/2))/N
[~,i]=max(z2)
Am=max(M(3:end))

M=f(i)
M(2)=Am
end
M(end+1)=dato
f0=M(1)
A=M(2)

